
<?php include_once('header.php') ?>
<div id="single-post">
    <div id="single-post-inner" class="container">
        <div class="col-md-9 list-into-single">
            <div>
                <p class="list-page-single"><a href="#">Home</a></p>>><p class="list-page-single"><a href="<?= base_url().'agenda' ?>">Agenda</a></p>
            </div>
        </div>
        <?php if (empty($post)): ?>
          <div class="col-md-9 single-post-posts" style="padding-bottom: 20px">
            <h2>Halaman belum ada</h2>
          </div>
        <?php else: ?>
          <div class="col-md-9 single-post-posts" style="padding-bottom: 20px">
              <div id="title-post">
                  <h2><?= $post[0]['judul'] ?></h2>
              </div>
              <div class="detail-post">
                  <p class="date-post">
                      <span class="glyphicon glyphicon-dashboard" style="margin-right:5px;color:#29CC6D"></span><b>Tanggal :</b>
                      <span class="text-date-post"><?= $post[0]['tanggal'] ?></span>
                  </p>
                  <p class="created-post" style="margin-right:10px">
                      <span class="glyphicon glyphicon-user"  style="margin-right:5px;color:#29CC6D"></span><b>Ditulis oleh : </b>
                      <span class="text-created-post"><?= $post[0]['penulis'] ?></span>
                  </p>
              </div>
              <p id="isi-post">
                 <?= $post[0]['content'] ?>
              </p>
          </div>
        <?php endif; ?>
        <div id="wrap-sidebar-single" class="col-md-3">
        <?php include("sidebar.php") ?>
      </div>
    </div>
</div>
<?php include_once('footer.php') ?>
