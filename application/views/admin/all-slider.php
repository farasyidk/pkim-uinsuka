

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color:#fff;overflow:auto;">
  <!-- Content Header (Page header) -->
  <div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px;padding-top: 50px">
    <div class="col-sm-12">
      <div class="box" style="padding-bottom:10px;border:none;box-shadow:none">
        <div class="box-header" style="padding-left:0px">
          <h1 class="box-title" style="display:block;font-weight:bold;font-size:2.3em">SEMUA SLIDER</h1>
          <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
          
          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <a href="#">
              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modaltambah"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Slider</button></a>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-bordered table-striped">
            <tr>
              <th>Gambar</th>
              <th>Judul</th>
              <th>Caption</th>
              <th>Action</th>
            </tr>
            <?php foreach ($tampil_slider as $tampil) {
              ?>
            <tr>
              <td><img src="<?php echo base_url(); ?>assets/img/slides/<?php echo $tampil->gambar; ?>" width="700px" height="300px"></td>
              <td><?php echo $tampil->status; ?></td>

              <td><?php echo $tampil->caption; ?></td>
              <td>
                <button type="button" class="btn btn-primary btn-edit" data-toggle="modal" data-target="#modaledit<?php echo $tampil->id ?>"><i class="fa fa-pencil" aria-hidden="true"></i></button>
           
                <button type="button" class="btn btn-danger btn-hapus"  data-toggle="modal" data-target="#modalhapus<?php echo $tampil->id ?>"><i class="fa fa-trash" aria-hidden="true"></i></button>
              
                </td>
            </tr>

            <div id="modalhapus<?php echo $tampil->id ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Menghapus Slider</h4>
      </div>
      <div class="modal-body">
        <center><h2>Yakin ingin Mengahapus !!</h2></center>
        <center>

      <a href="<?php echo base_url() ?>admin/delete_slides/<?php echo $tampil->id ?>">
        <button class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Hapus</button>
      </a>
        </center>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>




            <div id="modaledit<?php echo $tampil->id ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Slider</h4>
      </div>
      <div class="modal-body">
        <label>Judul : </label>
          <?php echo form_open_multipart('Admin/edit_slider');?>
  <form method="post" action="<?php echo base_url(); ?>Admin/edit_slider" name="kirimanlur" enctype="multipart/form-data">      <input type="hidden" name="id" value="<?php echo $tampil->id ?>">  
        <input type="hidden" name="gambar_lama" value="<?php echo $tampil->gambar ?>">
        <input type="text" name="judul" style="width: 100%" value="<?php echo $tampil->status ?>">
        <br>
        <br>
        <label>Caption : </label> 
        <input type="text" name="caption" style="width: 100%; height: 200px;" value="<?php echo $tampil->caption ?>">
        <br>
        <br>
        <label>Pilih Gambar : </label>
        <img src="<?php echo base_url() ?>assets/img/slides/<?php echo $tampil->gambar ?>" width="300px">
        <input type="file" name="gambar">
                        <br>
        <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-pencil" style="margin-right: 5px"></i>Simpan</button>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
            
            <?php } ?> 
            
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    
    <div class="col-sm-12 pagination-wrap">
   
    </div>


    <div id="modaltambah" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Slider</h4>
      </div>
      <div class="modal-body">
        <label>Judul : </label>
          <?php echo form_open_multipart('Admin/tambah_slider');?>
  <form method="post" action="<?php echo base_url(); ?>Admin/tambah_slider" name="kirimanlur" enctype="multipart/form-data">  
        <input type="text" name="judul" style="width: 100%" placeholder="Tulis Judul">
        <br>
        <br>
        <label>Caption : </label> 
        <input type="text" name="caption" style="width: 100%; height: 200px;" placeholder="Tulis Caption">
        <br>
        <br>
        <label>Pilih Gambar : </label>
        <input type="file" name="gambar">
                        <br>
        <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-pencil" style="margin-right: 5px"></i>Simpan</button>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
