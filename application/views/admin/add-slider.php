

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color:#fff;overflow:auto">
  <!-- Content Header (Page header) -->
  <div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
   <div class="col-sm-12" style="margin-top:15px;">
    <p  style="display:inline-block;"><a href="#">Home</a></p><span style="margin-left:5px;margin-right:5px;">>></span><p style="display:inline-block"><a href="#">Tambah Slider</a></p>
  </div>
  <div class="col-md-12">
    <div class="box-header">
      <h3>TAMBAH SLIDER POST</h3>
      <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
      <div class="form-group">
        <label for="judulPost">Judul Slider<sup style="color:red">*</sup></label>
        <br>
        <input type="text"  id="jdlPost" placeholder="Judul Slider">
      </div>
      <div class="box-body pad" style="padding:0px">
        <label for="judulPost">Isi Slider<sup style="color:red">*</sup></label>
        <form>
          <textarea id="editor1" name="editor1" rows="10" cols="80">

          </textarea>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="col-sm-12" style="margin-top:15px;">
 <div class="col-sm-3">
  <label for="imgPost" style="display:block">Gambar Post<sup style="color:red">*</sup></label>
  <input type="file"><br>
                    <span style="color:red">File gambar yang disarankan berekstensi .jpg/.jpeg dengan ukuran 1170 x 487 pixel</span>
</div>
</div>
<div class="col-sm-12" style="margin-bottom:15px;padding-left:15px;">
  <button style="margin-top:20px" class="btn btn-success">Submit</button>
  <button style="margin-top:20px" class="btn btn-danger">Batal</button>
</div>
<!-- /.content -->
</div>


