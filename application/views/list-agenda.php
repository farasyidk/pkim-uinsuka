
    <?php include_once('header.php') ?>
      <div id="list-post-wrap">
          <div class="container">
            <div class="col-md-9 list-into-single">
            <div>
                <p class="list-page-single"><a href="<?= base_url() ?>">Beranda</a></p>>>
                <p class="list-page"><a href="<?= base_url()."agenda" ?>">Agenda</a></p>
            </div>
        </div>
            <div class="col-md-9 single-post-posts">

                <div id="title-list-posts-wrap">
                    <h2 class="title-section" style="text-align:left">Agenda</h2>
                    <div class="underscore" style="margin-left:0px;margin-right:0px;"></div>
                </div>
              <ul style="padding-left:5px">
                <?php foreach ($daftar as $list): ?>
                  <li class="agenda-post-sidebar">
                    <a href="<?= base_url().'agenda/'.$list['slug'] ?>">
                      <i class="glyphicon glyphicon-calendar" style="margin-right:5px"></i>
                      <?= $list['judul'] ?>
                      <p class="date-agenda-sidebar"><?= $list['created_at'] ?></p>
                    </a>
                  </li>
                <?php endforeach; ?>
              </ul>
                <div class="col-sm-12 pagination-wrap">
                  <?= $this->pagination->create_links(); ?>
                     <!-- <ul class="pagination pagination-list-posts">
                      <li><a href="#">Previous</a></li>
                      <li class="active"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#">Next</a></li>
                    </ul> -->
                </div>
            </div>
            <div id="wrap-sidebar-single" class="col-md-3">
        <?php include("sidebar.php") ?>
    </div>
          </div>
      </div>
      <?php include_once('footer.php') ?>
