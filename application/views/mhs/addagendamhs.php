
      <div id="wrap-admin-mhs-outer">
        <div id="wrap-admin-mhs-inner" class="container">
            <h2 id="selamat-mhs">Tambah Agenda</h2>
            <div class="underscore" style="margin-left:0px;margin-right:0px"></div>
            <div class="col-sm-3">
               <?php include_once('sidebar.php') ?>
            </div>
            <div class="col-sm-9">
                <div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
                  <?php echo form_open_multipart('Pagemhs/kirim_agenda');?>
                <form method="post" action="<?php echo base_url(); ?>Pagemhs/kirim_agenda" name="kirimanlur" enctype="multipart/form-data">  
                        <div class="col-md-12">
                                <h3>TAMBAH AGENDA</h3>

                                <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
                                <div class="form-group">
                                        <label for="judulPost">Judul Agenda<sup style="color:red">*</sup></label>
                                        <input type="text" name="judul" class="form-control" id="jdlPost" placeholder="Judul Post">
                                </div>
                                <div class="box-body pad" style="padding:0px">
                                    <label for="judulPost">Isi Agenda<sup style="color:red">*</sup></label>
                                        
                                                <textarea id="editor4" name="content" rows="10" style="width: 100%">

                                                </textarea>
                                        
                                </div>
                        </div>
                  </div>
                  <div class="col-sm-12" style="margin-top:15px;">
                     <div class="col-sm-5">
                        <label for="judulPost">Tanggal Agenda<sup style="color:red">*</sup></label> 
                        <div class="form-group">
                          <input type="datetime-local" name="tgl_agenda">
                        </div> 
                     </div>
                  </div>
                  <div class="col-sm-12" style="margin-bottom:15px;">
                    <button style="margin-top:20px" name="status" value="1" type="Submit" class="btn btn-success">Submit</button>
                    <button style="margin-top:20px" name="status" value="0" type="Submit" class="btn btn-warning">Simpan di Draft</button>
                  </div>
            </div>
        </div>
      </div>
    </form>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor4');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
      });
    </script>
