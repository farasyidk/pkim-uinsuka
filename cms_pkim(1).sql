-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 07, 2018 at 07:16 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_pkim`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `id_adminn` text NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_adminn`, `username`, `password`) VALUES
(1, '1', 'admin', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id_agenda` int(5) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `nim` int(10) NOT NULL,
  `isi` varchar(300) NOT NULL,
  `created_at` date NOT NULL,
  `tgl_agenda` date NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `judul`, `nim`, `isi`, `created_at`, `tgl_agenda`, `status`) VALUES
(2, 'sasas', 16650065, '<p>sasasasa</p>\r\n', '1970-01-01', '2018-08-03', '1'),
(3, 'aki', 16650021, '<p>sasas</p>\r\n', '2018-08-02', '2018-08-22', '1'),
(4, 'sasa sasasas', 16650021, '<p>sasa</p>\r\n', '2018-08-02', '2018-08-14', '1');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `judul` varchar(125) NOT NULL,
  `content` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `slug` varchar(125) NOT NULL,
  `kategori` int(11) NOT NULL,
  `penulis` text NOT NULL,
  `viewer` int(11) NOT NULL,
  `suka` int(11) NOT NULL,
  `status_blog` varchar(1) NOT NULL,
  `komentar` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `judul`, `content`, `gambar`, `slug`, `kategori`, `penulis`, `viewer`, `suka`, `status_blog`, `komentar`, `created_at`, `updated_at`) VALUES
(2, 'Prodi BKI Gelar International Forum Discussion', 'Program Studi Bimbingan dan Konseling Islam UIN Sunan Kalijaga kembali menjalin kerjasama internasional dengan Universiti Sains Islam Malaysia (USIM). Kali ini sebanyak 30 orang mahasiswa Program Konspirasi                                                                                                ', 'e04e0455d0639647a1dba9180d8fa1b7.jpeg', 'prodi-bki-gelar-international-forum-discussion', 1, '16650021', 4, 2, '1', 0, '2018-07-15 17:00:00', '2018-07-16 04:42:29'),
(3, 'Pameran Media dan Alat Pembelajaran', 'Pameran media pembelajaran dan praktik edupreneurship PGMI terlaksana pada Sabtu, 18 September 2017 di Convention Hall UIN Sunan Kalijaga. selanjutnya                                                                                                ', '3b4b4979d1071f7e2577093c469ad216.jpg', 'pameran-media-dan-alat-pembelajaran', 2, '16650065', 2, 1, '1', 3, '2018-07-15 17:00:00', '2018-07-16 04:42:29'),
(5, 'Workshop Mengajar', 'Workshop Mengajar Akan Diadakan Tanggal 17 Agustus 2018. ', '', 'workshop-mengajar', 4, '16650065', 3, 1, '1', 0, '2018-07-16 17:00:00', '2018-07-17 11:53:23'),
(6, 'Kunjungan Industri', 'Kunjungan Industri Akan Diadakan Tanggal 17 Agustus 2018. ', '', 'kunjungan-industri', 4, '16650065', 0, 0, '1', 0, '2018-07-18 17:00:00', '2018-07-17 11:53:23'),
(7, 'Lomba Futsal PKIM', 'Lomba Futsal PKIM akan diadakan pada 02 Mei 2019', '', 'lomba-futsal-pkim', 4, '16650065', 0, 0, '1', 0, '2018-07-20 17:00:00', '2018-07-17 11:53:23'),
(25, 'Mencoba spasi dengan tgl', 'Mencoba spasi dengan tgl', '431e7fbbe3864f5914ea53a0a8bef560.png', 'Mencoba-spasi-dengan-tgl2018-08-0311-34-08', 1, '16650021', 0, 1, '1', 0, '2018-08-03 09:34:08', '2018-08-03 09:34:08'),
(26, 'admin1', '                      as', '8ca8d6832f014dee0e7ec8ce7cebd127.jpg', 'admin2018-08-1415-06-26', 1, '1', 0, 0, '1', 0, '2018-08-14 13:06:26', '2018-08-14 13:06:26'),
(27, 'asanos', '<p>sjaojs</p>\r\n', '75fec63d541fe5efb0411a69cb219fe3.jpeg', 'asanos2018-08-1415-55-14', 1, '16650075', 0, 0, '1', 0, '2018-08-14 13:55:14', '2018-08-14 13:55:14'),
(28, 'cek', '                                  sasas          ', '77324dd092344fa7e7f5910c49087842.jpg', 'meadoasjd2018-08-1417-42-49', 1, '1', 0, 0, '0', 0, '2018-08-14 15:42:49', '2018-08-14 15:42:49'),
(29, 'as', 'asa', 'asma', 'sasas', 4, '1', 0, 0, '1', 0, '2018-08-19 17:00:00', '2018-08-19 17:00:00'),
(30, 'sad', '<p>sad</p>\r\n', '', 'sad1970-01-0109-33-38', 4, '1', 0, 0, '1', 0, '2018-08-22 14:54:15', '2018-08-07 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id_halaman` int(11) NOT NULL,
  `judul_hlm` varchar(100) NOT NULL,
  `isi_hlm` varchar(600) NOT NULL,
  `waktu_tambah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `slug` varchar(200) NOT NULL,
  `ket` varchar(100) NOT NULL DEFAULT 'halaman'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halaman`
--

INSERT INTO `halaman` (`id_halaman`, `judul_hlm`, `isi_hlm`, `waktu_tambah`, `slug`, `ket`) VALUES
(1, 'sad ayyub', '<p>asd</p>\r\n', '2018-08-23 03:06:38', 'sad-ayyub', 'halaman'),
(3, 'sa', '<p>as</p>\r\n', '2018-08-22 15:21:42', 'sa', 'halaman'),
(4, 'Visi Misi', '<p>sda</p>\r\n', '2018-08-25 12:57:58', 'Visi-Misi', 'halaman');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `level` varchar(1) NOT NULL,
  `keterangan` varchar(100) NOT NULL DEFAULT 'kategori'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `level`, `keterangan`) VALUES
(1, 'Kelas-xx', '0', 'kategori'),
(2, 'Produk Skripsi', '0', 'kategori'),
(4, 'Agenda', '1', 'kategori'),
(5, 'Halaman', '1', 'kategori');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(11) NOT NULL,
  `komentar` text NOT NULL,
  `id_blogs` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `komentar`, `id_blogs`, `id_user`, `created_at`, `updated_at`, `token`) VALUES
(1, 'cek', 1, 16650065, '2018-08-15 12:21:10', '2018-08-15 12:21:10', '166500652018-08-1519-21-10');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` int(11) NOT NULL,
  `nama` varchar(125) NOT NULL,
  `foto` text NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama`, `foto`, `password`) VALUES
(16650021, 'Fadqurrosyidik', 'mee.jpg', '827ccb0eea8a706c4c34a16891f84e7b'),
(16650065, 'Ramadhan Ayub', 'ayub.jpg', '827ccb0eea8a706c4c34a16891f84e7b'),
(16650075, 'Kholiq Amrulloh', 'amrul.jpg', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nama` varchar(125) NOT NULL,
  `link` text NOT NULL,
  `child` int(11) NOT NULL,
  `header` int(11) NOT NULL,
  `sub_head` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `sub_menu` int(11) NOT NULL,
  `sub2_menu` int(11) NOT NULL,
  `sub3_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nama`, `link`, `child`, `header`, `sub_head`, `menu`, `sub_menu`, `sub2_menu`, `sub3_menu`) VALUES
(1, 'home', 'http://localhost/pkim/', 0, 1, 0, 1, 0, 0, 0),
(2, 'Identitas', '', 1, 1, 0, 2, 0, 0, 0),
(3, 'Visi - misi', 'http://localhost/pkim/visi-misi', 0, 0, 2, 0, 2, 0, 0),
(4, 'Profil Lulusan', 'http://localhost/pkim/profil-lulusan', 0, 0, 2, 0, 1, 0, 0),
(5, 'Akademik', '', 1, 1, 0, 3, 0, 0, 0),
(6, 'RPS', '', 1, 0, 5, 0, 1, 0, 0),
(7, 'Semester Gasal', '', 1, 0, 6, 0, 0, 1, 0),
(8, 'Semester Genap', '', 0, 0, 6, 0, 0, 2, 0),
(9, 'Matakuliah Pilihan', 'http://localhost/pkim/matakuliah-pilihan', 0, 0, 6, 0, 0, 3, 0),
(10, 'Semester 1', 'http://localhost/pkim/semester-1', 0, 0, 7, 0, 0, 0, 1),
(11, 'Semester 3', 'http://localhost/pkim/semester-3', 0, 0, 7, 0, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

CREATE TABLE `parent` (
  `id` int(11) NOT NULL,
  `nama_parent` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parent`
--

INSERT INTO `parent` (`id`, `nama_parent`) VALUES
(1, '0'),
(2, 'Profile'),
(6, 'Kegiatan');

-- --------------------------------------------------------

--
-- Table structure for table `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `id_pemberitahuan` int(5) NOT NULL,
  `nim` varchar(11) NOT NULL,
  `kategori_pemberitahuan` varchar(1) NOT NULL,
  `slug_pemberitahuan` varchar(300) NOT NULL,
  `tgl_pemberitahuan` datetime NOT NULL,
  `status` varchar(1) NOT NULL,
  `token` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemberitahuan`
--

INSERT INTO `pemberitahuan` (`id_pemberitahuan`, `nim`, `kategori_pemberitahuan`, `slug_pemberitahuan`, `tgl_pemberitahuan`, `status`, `token`) VALUES
(47, '16650065', '2', 'pameran-media-dan-alat-pembelajaran', '2018-08-05 15:51:12', '1', '166500212018-08-0515-51-12'),
(53, '16650065', '2', 'workshop-mengajar', '2018-08-05 15:54:02', '1', '166500212018-08-0515-54-02'),
(54, '16650021', '2', 'prodi-bki-gelar-international-forum-discussion', '2018-08-05 16:02:46', '1', '166500652018-08-0516-02-46'),
(55, '16650021', '2', 'prodi-bki-gelar-international-forum-discussion', '2018-08-05 16:03:10', '1', '166500212018-08-0516-03-10'),
(56, '16650021', '1', 'mantap2018-08-0814-48-04', '2018-08-08 14:48:04', '1', ''),
(57, '16650021', '1', 'mantap2018-08-0814-49-35', '2018-08-08 14:49:35', '1', ''),
(58, '16650021', '1', 'mantap2018-08-0814-51-50', '2018-08-08 14:51:50', '1', ''),
(59, '16650021', '2', 'mantap2018-08-0814-51-50', '2018-08-08 15:18:14', '1', '166500212018-08-0815-18-14'),
(60, '1', '1', 'admin2018-08-1415-06-26', '2018-08-14 15:06:26', '0', ''),
(61, '1', '1', 'asanos2018-08-1415-55-14', '2018-08-14 15:55:14', '0', ''),
(62, '1', '1', 'meadoasjd2018-08-1417-42-49', '2018-08-14 17:42:49', '0', ''),
(64, '16650021', '2', 'Mencoba-spasi-dengan-tgl2018-08-0311-34-08', '2018-09-05 21:27:24', '0', '166500652018-09-0521-27-24');

-- --------------------------------------------------------

--
-- Table structure for table `single-page`
--

CREATE TABLE `single-page` (
  `id` int(11) NOT NULL,
  `judul` varchar(125) NOT NULL,
  `content` text NOT NULL,
  `penulis` varchar(100) NOT NULL,
  `slug` varchar(125) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `caption` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `caption`, `gambar`, `status`) VALUES
(1, 'Beasiswa tentu menjadi hal yang didambakan bagi para mahasiswa dalam menempuh kuliah.', '4945b5fb41f6484e3329e50f7a18baf2.jpg', 'coba2'),
(2, 'Beasiswa tentu menjadi hal yang didambakan bagi para mahasiswa dalam menempuh kuliah.', 'bd0e8af8d40eb640c6af82aeeb9e97b9.jpg', 'Heading');

-- --------------------------------------------------------

--
-- Table structure for table `suka`
--

CREATE TABLE `suka` (
  `id` int(11) NOT NULL,
  `id_blogs` varchar(200) NOT NULL,
  `nim` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suka`
--

INSERT INTO `suka` (`id`, `id_blogs`, `nim`, `created_at`, `token`) VALUES
(24, '3', 16650021, '2018-08-05 13:51:12', '166500212018-08-0515-51-12'),
(25, '5', 16650021, '2018-08-05 13:54:02', '166500212018-08-0515-54-02'),
(26, '2', 16650065, '2018-08-05 14:02:46', '166500652018-08-0516-02-46'),
(27, '2', 16650021, '2018-08-05 14:03:10', '166500212018-08-0516-03-10'),
(29, '25', 16650065, '2018-09-05 14:27:24', '166500652018-09-0521-27-24');

-- --------------------------------------------------------

--
-- Table structure for table `tata`
--

CREATE TABLE `tata` (
  `id_tata_letak` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(100) NOT NULL,
  `status_parent` int(11) DEFAULT '0',
  `nama_parent` varchar(100) DEFAULT NULL,
  `urut` int(11) NOT NULL,
  `status_menu` int(11) DEFAULT '0',
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tata`
--

INSERT INTO `tata` (`id_tata_letak`, `id_menu`, `nama_menu`, `status_parent`, `nama_parent`, `urut`, `status_menu`, `keterangan`) VALUES
(28, 2, 'Produk Skripsi', 1, 'Profile', 0, 0, 'kategori'),
(30, 1, 'Kelas-xx', 1, 'Profile', 0, 0, 'kategori'),
(31, 2, 'Profile', 2, 'Profile', 5, 1, 'parent'),
(32, 3, 'sa', 0, '-- Tidak Ada Parent --', 8, 1, 'halaman'),
(35, 1, 'sad ayyub', 0, '-- Tidak Ada Parent --', 4, 1, 'halaman'),
(36, 6, 'Kegiatan', 0, 'Kegiatan', 0, 0, 'parent'),
(37, 4, 'Visi Misi', 1, 'Profile', 0, 0, 'halaman');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id_halaman`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parent`
--
ALTER TABLE `parent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`id_pemberitahuan`);

--
-- Indexes for table `single-page`
--
ALTER TABLE `single-page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suka`
--
ALTER TABLE `suka`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tata`
--
ALTER TABLE `tata`
  ADD PRIMARY KEY (`id_tata_letak`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id_agenda` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id_halaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `parent`
--
ALTER TABLE `parent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  MODIFY `id_pemberitahuan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `single-page`
--
ALTER TABLE `single-page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `suka`
--
ALTER TABLE `suka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tata`
--
ALTER TABLE `tata`
  MODIFY `id_tata_letak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
